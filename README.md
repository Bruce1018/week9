# Week9-mini-proj

> zx112  
> Bruce Xu

# Project Demo

This time i used a model called `Sentiment Analysis` from `Hugging face`

After you text a plain text, the system will return the corresponding sentiment

**plain text**

Under the soft glow of the moonlight, I stood alone by the serene lake, the gentle breeze whispering secrets of the night. Memories flooded in, bittersweet, each one a delicate thread in the intricate tapestry of my life. Joy mingled with sorrow, a poignant reminder of the ephemeral nature of our existence. In that moment, I felt a profound connection to everything around me, a deep sense of peace amidst the swirling tides of emotion. It was a reminder that even in solitude, we are never truly alone, for the beauty of the world and the richness of our experiences bind us together in the shared journey of life.

**result**

![](/截屏2024-04-07%2022.50.29.png)

# requirements

## Create a website using Streamlit

0. install `python` using `homebrew`

`brew install python`

1. set up virtual environment by following commands

`python3 -m venv /Users/xzl/Desktop/week9-mini-proj/env    `

2. activate the `venv` using following command

`source /Users/xzl/Desktop/week9-mini-proj/env/bin/activate`

3. install the `python package` we need

`pip install streamlit transformers`

## Connect to an open source LLM (Hugging Face)

create the `app.py` and edit it

## Deploy model via Streamlit or other service (accessible via browser)

0. Login in `Streamelit`

1. Create Github repo

2. Connect the repo and deploy it

![](/截屏2024-04-07%2022.46.04.png)



